const path = require("path")
const fs = require("fs")
const dbDir = path.join(__dirname, "../database")
const dbFile = path.join(dbDir, "post.json")

function getDatabase() {
    if (!fs.existsSync(dbDir)) {
        fs.mkdirSync(dbDir)
    }
    
    if (!fs.existsSync(dbFile)) {
        fs.writeFileSync(dbFile, "[]")
    }
    
    const data = fs.readFileSync(dbFile)

    return JSON.parse(data.toString())
}

function writeDatabase(data) {
    if (!fs.existsSync(dbDir)) {
        fs.mkdirSync(dbDir)
    }
    
    fs.writeFileSync(dbFile, JSON.stringify(data, null, 2))
}

function createPost(data) {
    // ambil data lama
    let posts = getDatabase()

    // buatkan id generated by system
    data.id = new Date().getTime()

    // tambah data baru
    posts.unshift(data)

    // simpan
    writeDatabase(posts)
}

function readPost(id) {
    // ambil data lama
    let posts = getDatabase()

    // temukan data yang sesuai dengan id-nya
    let post = posts.find(item => item.id == id)

    // return data yang di temukan
    return post
}

function updatePost(id, newData) {
    // ambil data lama
    let posts = getDatabase()

    // ganti data sesuai dengan id yang ditemukan
    let newPosts = posts.map(oldData => {
        if (oldData.id == id) {
            return Object.assign(oldData, newData)
        }

        return oldData
    })

    // simpan
    writeDatabase(newPosts)
}

function deletePost(id) {
    // ambil data lama
    let posts = getDatabase()

    // ambil semua data kecuali data yang id-nya mau dihapus
    let newData = posts.filter(oldData => oldData.id != id)

    // simpan
    writeDatabase(newData)
}

module.exports = {
    createPost,
    readPost,
    updatePost,
    deletePost,
    getDatabase
}