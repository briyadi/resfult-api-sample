const express = require("express")
const router = express.Router()
const postData = require("../services/post_data")

router.use(express.json())

// menampilkan seluruh data posts
router.get("/", (req, res) => {
    res.json(postData.getDatabase())
})

// menampilkan 1 data post sesuai id yang diminta
router.get("/:id", (req, res) => {
    let oldData = postData.readPost(req.params.id)

    if (!oldData) {
        res.status(404).json({
            message: "data not found"
        })
        return
    }

    res.json(oldData)
})

// membuat data baru
router.post("/", (req, res) => {
    let newData = {
        title: req.body.title,
        body: req.body.body
    }

    postData.createPost(newData)

    res.status(201).json({
        message: "new data created"
    })
})

// memperbarui data yang sudah ada sesuai id yang dikirim
router.put("/:id", (req, res) => {
    let oldData = postData.readPost(req.params.id)
    if (!oldData) {
        res.status(404).json({
            message: "data not found"
        })
        return
    }

    let newData = {
        title: req.body.title,
        body: req.body.body
    }

    postData.updatePost(req.params.id, newData)

    res.status(201).json({
        message: "old data updated"
    })
})

// menghapus data yang sudah ada sesuai id yang dikirim
router.delete("/:id", (req, res) => {
    let oldData = postData.readPost(req.params.id)
    if (!oldData) {
        res.status(404).json({
            message: "data not found"
        })
        return
    }

    postData.deletePost(req.params.id)

    res.status(201).json({
        message: "old data removed"
    })
})

module.exports = router